<?php

/**
 * @file
 * Look for the views conference schedule view for basic_conferences module.
 */

/**
 * Implements hook_views_default_views().
 */
function basic_conferences_views_default_views() {
  // Make sure that the 'view' class is defined.
  views_include("view");
  $files = file_scan_directory(drupal_get_path('module', 'event_conferences') . '/includes/views', '.view');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}
