<?php

/**
 * @file
 * Install Session Content Type for the basic_conferences module.
 */

$content['type'] = array(
  'name' => 'Session',
  'type' => 'session',
  'description' => 'A session is part of a conference.',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array(
    'status' => TRUE,
    'promote' => TRUE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'language_content_type' => '0',
  'upload' => '1',
  'old_type' => 'session',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'nodewords_edit_metatags' => 1,
  'nodewords_metatags_generation_method' => '0',
  'nodewords_metatags_generation_source' => '2',
  'nodewords_use_alt_attribute' => 1,
  'nodewords_filter_modules_output' =>
  array(
    'imagebrowser' => FALSE,
    'img_assist' => FALSE,
  ),
  'nodewords_filter_regexp' => '',
  'print_display' => 1,
  'print_display_comment' => 0,
  'print_display_urllist' => 1,
  'print_pdf_display' => 1,
  'print_pdf_display_comment' => 0,
  'print_pdf_display_urllist' => 1,
  'i18n_newnode_current' => 0,
  'i18n_required_node' => 0,
  'i18n_lock_node' => 0,
  'i18n_node' => 1,
);
$content['fields'] = array(
  0 =>
  array(
    'label' => 'Session Date',
    'field_name' => 'field_session_date',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '-4',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_session_date][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' =>
    array(
      'session' => 'session_time',
      'conference' => 0,
      'page' => 0,
      'speaker' => 0,
      'room' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array(
      'nid' =>
      array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'index' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 =>
  array(
    'label' => 'Show Session Time',
    'field_name' => 'field_session_time_display',
    'type' => 'number_integer',
    'widget_type' => 'optionwidgets_onoff',
    'change' => 'Change basic information',
    'weight' => '-2',
    'description' => 'Check if you need to hide the session time. Note: the session time will still be respected for sorting.',
    'default_value' =>
    array(
      0 =>
      array(
        'value' => 1,
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_session_time_display' =>
      array(
        'value' => TRUE,
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'min' => '',
    'max' => '',
    'prefix' => '',
    'suffix' => '',
    'allowed_values' => '0|Display session times, 1|Hide session times',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'number',
    'widget_module' => 'optionwidgets',
    'columns' =>
    array(
      'value' =>
      array(
        'type' => 'int',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      5 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  2 =>
  array(
    'label' => 'Topic',
    'field_name' => 'field_session_topic',
    'type' => 'text',
    'widget_type' => 'optionwidgets_select',
    'change' => 'Change basic information',
    'weight' => '-1',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_session_topic' =>
      array(
        'value' =>
        array(
          0 => '',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '1|Topic one, 2|Topic two, 3|Topic three, 4|Topic four, 5|Topic five',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'optionwidgets',
    'columns' =>
    array(
      'value' =>
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
        'views' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      5 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  3 =>
  array(
    'label' => 'Room',
    'field_name' => 'field_room',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'change' => 'Change basic information',
    'weight' => 0,
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'nid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_room' =>
      array(
        0 =>
        array(
          'nid' => '',
        ),
        'nid' =>
        array(
          'nid' =>
          array(
            0 => '',
          ),
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' =>
    array(
      'room' => 'room',
      'page' => 0,
      'session' => 0,
      'speakers' => 0,
      'webform' => 0,
      'conference' => FALSE,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array(
      'nid' =>
      array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'index' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'weight' => '-3',
      'parent' => '',
      'label' =>
      array(
        'format' => 'above',
      ),
      'teaser' =>
      array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  4 =>
  array(
    'label' => 'Moderators',
    'field_name' => 'field_moderators',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '1',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => 'Sessions can contain multiple moderators if required - moderators will appear above "speakers" on the conference screen',
    'default_value' =>
    array(
      0 =>
      array(
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_moderators][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_moderators' =>
      array(
        0 =>
        array(
          'nid' =>
          array(
            'nid' => '',
            '_error_element' => 'default_value_widget][field_moderators][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_moderators][0][nid][nid',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' =>
    array(
      'speakers' => 'speakers',
      'page' => 0,
      'room' => 0,
      'session' => 0,
      'webform' => 0,
      'conference' => FALSE,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array(
      'nid' =>
      array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'index' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  5 =>
  array(
    'label' => 'Speakers',
    'field_name' => 'field_speakers',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'change' => 'Change basic information',
    'weight' => '2',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_speakers][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_speakers' =>
      array(
        0 =>
        array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_speakers][0][nid][nid',
        ),
        'nid' =>
        array(
          'nid' =>
          array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' =>
    array(
      'speakers' => 'speakers',
      'page' => 0,
      'room' => 0,
      'session' => 0,
      'conference' => FALSE,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array(
      'nid' =>
      array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'index' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra'] = array(
  'title' => '-5',
  'body_field' => '-4',
  'revision_information' => '6',
  'author' => '5',
  'options' => '7',
  'menu' => '3',
  'path' => '8',
  'attachments' => '10',
  'print' => '9',
  'nodewords' => '4',
);
