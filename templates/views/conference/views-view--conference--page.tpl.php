<?php
/**
 * @file
 * Main view template for conference schedule page.
 */
?>
<div>
  <?php if ($admin_links): ?>
      <?php print $admin_links; ?>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php endif; ?>
</div> <?php /* class view */ ?>
