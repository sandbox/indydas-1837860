<?php

/**
 * @file
 * Install Room Content Type for the basic_conferences module.
 */

$content['type'] = array(
  'name' => 'Room',
  'type' => 'room',
  'description' => 'A room is a part of a conference',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array(
    'status' => TRUE,
    'promote' => FALSE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'upload' => '0',
  'ffp_upload' =>
  array(
    'file_path' => '',
    'file_path_cleanup' =>
    array(
      'file_path_pathauto' => 0,
      'file_path_tolower' => 0,
      'file_path_transliterate' => 0,
    ),
    'file_name' => '[filefield-onlyname-original].[filefield-extension-original]',
    'file_name_cleanup' =>
    array(
      'file_name_pathauto' => 0,
      'file_name_tolower' => 0,
      'file_name_transliterate' => 0,
    ),
    'retroactive_update' => '',
    'active_updating' => 0,
  ),
  'old_type' => 'stream',
  'orig_type' => 'stream',
  'custom' => '0',
  'modified' => '1',
  'locked' => '1',
  'reset' => 'Reset to defaults',
);
$content['fields'] = array(
  0 =>
  array(
    'label' => 'Conference',
    'field_name' => 'field_conference',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '-4',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_conference][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' =>
    array(
      'conference' => 'conference',
      'page' => 0,
      'session' => 0,
      'speaker' => 0,
      'room' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array(
      'nid' =>
      array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'index' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 =>
  array(
    'label' => 'Weight',
    'field_name' => 'field_weight',
    'type' => 'number_integer',
    'widget_type' => 'optionwidgets_select',
    'change' => 'Change basic information',
    'weight' => '-3',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'value' => '0',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_weight' =>
      array(
        'value' => '0',
      ),
    ),
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'min' => '',
    'max' => '',
    'prefix' => '',
    'suffix' => '',
    'allowed_values' => '10, 9, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'number',
    'widget_module' => 'optionwidgets',
    'columns' =>
    array(
      'value' =>
      array(
        'type' => 'int',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
