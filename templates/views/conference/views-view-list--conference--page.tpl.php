<?php
/**
 * @file
 * Views template for conference schedule page row output from list.
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print t($title); ?></h3>
<?php endif; ?>
<<?php print $options['type']; ?>>
<?php
  // Start with 0 to begin with so we can count on this.
  $count = 0;
?>
<?php foreach ($rows as $id => $row):
  // We will divide the row count by two then apply zebra as class.
  $zebra = ($count % 2) ? 'odd' : 'even';
?>
  <li class="session-row <?php print $zebra ?>">
    <?php print $row; ?>
  </li>
  <?php
    // Within the loop increment a count to each row
    // to determine if odd or even number.
    $count++;
  ?>
<?php endforeach; ?>
</<?php print $options['type']; ?>>
