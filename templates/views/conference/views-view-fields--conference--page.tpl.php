<?php
/**
 * @file
 * Views template for conference schedule page row.
 */
?>
<ul>
  <li class="title"><?php print $fields['title']->content; ?></li>
  <li class="room"><?php print $fields['field_stream_nid']->content; ?></li>
</ul>
