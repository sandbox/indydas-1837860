<?php
/**
 * @file
 * node-conference.tpl.php - node template for conference nodes.
 */
?>

<div<?php print $attributes; ?>>

  <?php print $picture ?>

  <?php if (!$page && $title): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <?php if ($submitted):?>
  <div class="submitted"><?php print $submitted ?></div>
  <?php endif; ?>
  

  <?php print $content ?>
  <?php if ($terms):?>
  <div class="taxonomy"><?php print $terms; ?></div>
  <?php endif; ?>
  <?php if ($links):?>
  <div class="node-links"><?php print $links; ?></div>
  <?php endif; ?>
</div>
