Description
-----------

Event Conferences - Use a simple conferencing facility in any Drupal site

Installation
------------

- Install the Event Conferences module the usual way at admin/build/modules.

- Copy the views, node template files from the templates folder into your theme.

- Enable the conference view.

- Create conference, room, session, session_time and speaker content.

Once enabled feel free to amend the conference view to match your needs.
