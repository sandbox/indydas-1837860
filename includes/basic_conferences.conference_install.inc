<?php

/**
 * @file
 * Install Conference Content Type for the basic_conferences module.
 */

$content['type'] = array(
  'name' => 'Conference',
  'type' => 'conference',
  'description' => 'A conference',
  'title_label' => 'Title',
  'body_label' => 'Description',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array(
    'status' => TRUE,
    'promote' => TRUE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'upload' => '0',
  'ffp_upload' =>
  array(
    'file_path' => '',
    'file_path_cleanup' =>
    array(
      'file_path_pathauto' => 0,
      'file_path_tolower' => 0,
      'file_path_transliterate' => 0,
    ),
    'file_name' => '[filefield-onlyname-original].[filefield-extension-original]',
    'file_name_cleanup' =>
    array(
      'file_name_pathauto' => 0,
      'file_name_tolower' => 0,
      'file_name_transliterate' => 0,
    ),
    'retroactive_update' => '',
    'active_updating' => 0,
  ),
  'old_type' => 'conference',
  'orig_type' => 'conference',
  'custom' => '0',
  'modified' => '1',
  'locked' => '1',
  'reset' => 'Reset to defaults',
);

$content['extra'] = array(
  'title' => '-5',
  'body_field' => '-2',
  'revision_information' => '2',
  'author' => '1',
  'options' => '3',
  'menu' => '-3',
  'path' => '5',
  'attachments' => '4',
);
