<?php

/**
 * @file
 * Install Speaker Content Type for the basic_conferences module.
 */

$content['type'] = array(
  'name' => 'Speaker',
  'type' => 'speaker',
  'description' => '',
  'title_label' => 'Speaker name',
  'body_label' => 'Speaker Bio',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array(
    'status' => TRUE,
    'promote' => FALSE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'upload' => '0',
  'ffp_upload' =>
  array(
    'file_path' => '',
    'file_path_cleanup' =>
    array(
      'file_path_pathauto' => 0,
      'file_path_tolower' => 0,
      'file_path_transliterate' => 0,
    ),
    'file_name' => '[filefield-onlyname-original].[filefield-extension-original]',
    'file_name_cleanup' =>
    array(
      'file_name_pathauto' => 0,
      'file_name_tolower' => 0,
      'file_name_transliterate' => 0,
    ),
    'retroactive_update' => '',
    'active_updating' => 0,
  ),
  'old_type' => 'speaker',
  'orig_type' => 'speaker',
  'custom' => '0',
  'modified' => '1',
  'locked' => '1',
  'reset' => 'Reset to defaults',
);
$content['fields'] = array(
  0 =>
  array(
    'label' => 'Speaker role',
    'field_name' => 'field_speaker_role',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-4',
    'rows' => 5,
    'size' => '60',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_speaker_role][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' =>
    array(
      'value' =>
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
        'views' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'weight' => '-4',
      'parent' => '',
      4 =>
      array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      'label' =>
      array(
        'format' => 'hidden',
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 =>
  array(
    'label' => 'Speaker Image',
    'field_name' => 'field_primary_image',
    'type' => 'filefield',
    'widget_type' => 'imagefield_widget',
    'change' => 'Change basic information',
    'weight' => '-3',
    'file_extensions' => 'png gif jpg jpeg',
    'progress_indicator' => 'bar',
    'file_path' => '',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'max_resolution' => 0,
    'min_resolution' => 0,
    'custom_alt' => 1,
    'alt' => '',
    'custom_title' => 1,
    'title_type' => 'textfield',
    'title' => '',
    'use_default_image' => 0,
    'default_image_upload' => '',
    'default_image' => NULL,
    'resolution' => '150x150',
    'enforce_ratio' => 1,
    'enforce_minimum' => 1,
    'croparea' => '500x500',
    'description' => '',
    'ffp_field_primary_image' =>
    array(
      'file_path' => 'speaker',
      'file_path_cleanup' =>
      array(
        'file_path_pathauto' => 0,
        'file_path_tolower' => 0,
        'file_path_transliterate' => 0,
      ),
      'file_name' => '[filefield-onlyname-original].[filefield-extension-original]',
      'file_name_cleanup' =>
      array(
        'file_name_pathauto' => 0,
        'file_name_tolower' => 0,
        'file_name_transliterate' => 0,
      ),
      'retroactive_update' => '',
      'active_updating' => 0,
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'op' => 'Save field settings',
    'module' => 'filefield',
    'widget_module' => 'imagefield',
    'columns' =>
    array(
      'fid' =>
      array(
        'type' => 'int',
        'not null' => FALSE,
        'views' => TRUE,
      ),
      'list' =>
      array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'views' => TRUE,
      ),
      'data' =>
      array(
        'type' => 'text',
        'serialize' => TRUE,
        'views' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'label' =>
      array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      5 =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
    ),
  ),
  2 =>
  array(
    'label' => 'Speaker company URL',
    'field_name' => 'field_speaker_company',
    'type' => 'link',
    'widget_type' => 'link',
    'change' => 'Change basic information',
    'weight' => '-2',
    'description' => '',
    'default_value' =>
    array(
      0 =>
      array(
        'title' => '',
        'url' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array(
      'field_speaker_company' =>
      array(
        0 =>
        array(
          'title' => '',
          'url' => '',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'validate_url' => 1,
    'url' => 'optional',
    'title' => 'required',
    'title_value' => '',
    'enable_tokens' => 0,
    'display' =>
    array(
      'url_cutoff' => '80',
    ),
    'attributes' =>
    array(
      'target' => '_blank',
      'rel' => '',
      'class' => '',
      'title' => '',
    ),
    'op' => 'Save field settings',
    'module' => 'link',
    'widget_module' => 'link',
    'columns' =>
    array(
      'url' =>
      array(
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
      'title' =>
      array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
      'attributes' =>
      array(
        'type' => 'text',
        'size' => 'medium',
        'not null' => FALSE,
      ),
    ),
    'display_settings' =>
    array(
      'weight' => '-2',
      'parent' => '',
      4 =>
      array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      'label' =>
      array(
        'format' => 'hidden',
      ),
      'teaser' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
